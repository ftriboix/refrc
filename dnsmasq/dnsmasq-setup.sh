#!/bin/bash

set -euo pipefail

sudo apt update -y
sudo apt install -y dnsmasq
sudo apt clean -y

echo 'server=9.9.9.9' | sudo tee -a /etc/dnsmasq.d/my-dns-servers.conf
echo 'server=1.1.1.1' | sudo tee /etc/dnsmasq.d/my-dns-servers.conf

sudo systemctl stop systemd-resolved || true
sudo systemctl disable systemd-resolved || true

# `/etc/resolv.conf` is likely a symbolic link to
# `../run/systemd/resolve/stub-resolv.conf`
sudo rm -f /etc/resolv.conf

echo 'nameserver 127.0.0.1' | sudo tee /etc/resolv.conf

sudo systemctl enable dnsmasq
sudo systemctl restart dnsmasq
