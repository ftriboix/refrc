#!/bin/bash

set -euo pipefail

rm -rf ~/.vimrc ~/.vim

pack_dir=~/.vim/pack/dist/start
mkdir -p "$pack_dir"
git clone https://github.com/rafi/awesome-vim-colorschemes.git "$pack_dir/awecome-vim-colorschemes"
git clone https://github.com/preservim/nerdtree.git "$pack_dir/nerdtree"
git clone https://github.com/vim-airline/vim-airline "$pack_dir/vim-airline"
git clone https://github.com/vim-airline/vim-airline-themes "$pack_dir/vim-airline-themes"
git clone https://github.com/sheerun/vim-polyglot.git "$pack_dir/vim-polyglot"

cp vimrc ~/.vimrc
