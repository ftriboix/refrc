To install Neovim and NvChad, just run `./install-nvchad.sh` in this directory.
Please note this is destructive to your old Neovim installation. You probably
want to back your old Neovim installation up like this:

```sh
$ mv ~/.config/nvim ~/.config/nvim.backup
$ mv ~/.local/share/nvim ~/.local/share/nvim.backup
$ mv ~/.local/state/nvim ~/.local/state/nvim.backup
$ mv ~/.cache/nvim ~/.cache/nvim.backup
```
