return {
  {
    "farmergreg/vim-lastplace",
    lazy = false,
  },

  {
    "hrsh7th/nvim-cmp",

    opts = function()
      local cmp = require("cmp")
      local conf = require("nvchad.configs.cmp")

      local mymappings = {
        ["<Up>"] = cmp.mapping.select_prev_item(),
        ["<Down>"] = cmp.mapping.select_next_item(),
        ["<Tab>"] = cmp.mapping.confirm {
          behavior = cmp.ConfirmBehavior.Replace,
          select = true,
        },
      }
      conf.mapping = vim.tbl_deep_extend("force", conf.mapping, mymappings)
      return conf
    end,
  },

  {
    "nvim-treesitter/nvim-treesitter",

    opts = {
      ensure_installed = {
        "bash",
        "comment",
        "diff",
        "dockerfile",
        "helm",
        "json",
        "lua",
        "markdown",
        "markdown_inline",
        "python",
        "terraform",
        "vim",
        "vimdoc",
        "yaml",
      },
      indent = {
        enabled = true
      },
    },
  },

  {
    "williamboman/mason.nvim",

    opts = {
      ensure_installed = {
        "actionlint",
        "ansible-language-server",
        "ansible-lint",
        "bash-language-server",
        "docker-compose-language-service",
        "dockerfile-language-server",
        "helm-ls",
        "lua-language-server",
        "luacheck",
        "luaformatter",
        "marksman",
--        "misspell",  -- Failed to build
--        "nginx-language-server",  -- Fails to install
        "pydocstyle",
        "pyflakes",
        "pylint",
        "pyre",
        "pyright",
        "python-lsp-server",
        "sqlfmt",
        "sqlls",
        "systemdlint",
        "terraform-ls",
        "tflint",
        "vim-language-server",
        "vint",
        "yaml-language-server",
        "yamlfix",
        "yamlfmt",
      },
    },
  },
}
