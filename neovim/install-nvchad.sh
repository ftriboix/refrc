#!/bin/bash

set -euo pipefail

tmpdir=$(mktemp -d)
echo "### Created temporary directory $tmpdir"
cp myplugins.lua "${tmpdir}/"
cd "$tmpdir"

function cleanup() {
  echo "CLEANUP: Removing temporary directory $tmpdir"
  rm -rf "$tmpdir"
}

trap cleanup EXIT

echo "### Installing dependencies"
sudo apt-get update
sudo apt-get install -y \
  build-essential \
  cmake \
  curl \
  git \
  jq \
  luarocks \
  python3.12-venv \
  ripgrep \
  unzip \
  xclip

echo "### Installing NodeJS"
curl -sSL -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
export NVM_DIR="$HOME/.nvm"
source "$NVM_DIR/nvm.sh"
nvm install v22.1.0

echo "### Downloading JetBrainsMono fonts"
curl -LO https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMono.zip

echo "### Installing JetBrainsMono fonts"
unzip JetBrainsMono.zip > /dev/null
mkdir -p ~/.local/share/fonts
mv -f JetBrainsMonoNLNerdFont-Regular.ttf JetBrainsMonoNLNerdFont-SemiBold.ttf ~/.local/share/fonts
fc-cache -fv > /dev/null

neovim_version=$(curl -sSL https://api.github.com/repos/neovim/neovim/releases/latest | jq -r .tag_name)
echo "### Installing Neovim version $neovim_version"
curl -sSLO "https://github.com/neovim/neovim/releases/download/${neovim_version}/nvim.appimage"
chmod +x nvim.appimage
mkdir -p ~/.local/bin
mv -f nvim.appimage ~/.local/bin/nvim

echo "### Installing NvChad"
rm -rf ~/.config/nvim
rm -rf ~/.local/share/nvim
rm -rf ~/.local/state/nvim
rm -rf ~/.cache/nvim
git clone https://github.com/NvChad/starter ~/.config/nvim --depth 1

echo "### Customising NvChad"
cp -f myplugins.lua ~/.config/nvim/lua/plugins
sed -i 's/onedark/github_light/' ~/.config/nvim/lua/chadrc.lua
cat <<EOF >> ~/.config/nvim/lua/mappings.lua

-- Custom mappings
map("n", "<C-Left>", "<C-w>h", { desc = "Go to window left" })
map("n", "<C-Right>", "<C-w>l", { desc = "Go to window right" })
map("n", "<C-Down>", "<C-w>j", { desc = "Go to window down" })
map("n", "<C-Up>", "<C-w>k", { desc = "Go to window up" })
EOF

echo
echo "Neovim and NvChad successfully installed and configured."
echo
echo "Here is what you need to do now:"
echo "  - Configure your terminal emulator to use one of the JetBrainsMono fonts"
echo "  - Open Neovim and wait for NvChad to finish installing"
echo '  - Once NvChad finished installing, run `:MasonInstallAll`'
echo "  - That's it! Enjoy!"
echo
