# .bash_profile

# User specific environment and startup programs

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

PATH="$HOME/.local/bin:$PATH"
